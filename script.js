// Query Operators
// Allow us to expand our queries and define conditions instead of just looking for specific values.
// $gt - greater than
db.products.find({price:{$gt:3000}})

// $lt - less than
db.products.find({price:{$lt:3000}})

// $gte - greater than or equal to
db.products.find({price:{$gte:30000}})

// $lte - less than or equal to
db.products.find({price:{$lte:2800}})

//$regex - query operator which allow us to find documents will match characters we are looking for
//$regex - to look for documents with a partial match by default is case sensitive:
db.users.find({firstName:{$regex: 'o'}})

//$options: $i - will make our regex case insensitive
db.users.find({firstName:{$regex:'e',$options: '$i'}})

//We can also use $regex for partial matches
db.products.find({name:{$regex: 'phone', $options: '$i'}})

db.users.find({email:{$regex: 'web', $options: '$i'}})

db.products.find({name: {$regex: 'razer', $options: '$i'}})

db.products.find({name:{$regex: 'rakk',$options: '$i'}})

//$or $and - logical operators and they work almost the same as in JS
//$or - allows us to set a condition to find for documents which can satisfy at least 1 of the given conditions.

db.products.find({$or:[{name:{$regex: 'x', $options: '$i'}},{price:{$lte:10000}}]})

db.products.find({
    $or:[
        {name:{$regex: 'x',$options: '$i'}},
        {price:{$gte:30000}}
        ]        
})

//$and - look or find documents that satisfies both conditions:
db.products.find({
    $and:[
          {name:{$regex: 'razor',$options: '$i'}},
          {price:{$gte:3000}}
         ]   
})

db.users.find({
    $and:[
        {lastName:{$regex:'a',$options: '$i'}},
        {isAdmin:true}
       ]
})

// Field Projection - allows us to hide/show certain fields and properties of documents that were retrieved

//hide its id and password with 0
db.users.find({},{_id:0,password:0})

//field projection = 0 means hide, 1 means show
//By default we have to explicitly hide the id field.
db.users.find({isAdmin:true},{_id:0,email:1})

db.users.find({isAdmin:false},{firstName:1,lastName:1})